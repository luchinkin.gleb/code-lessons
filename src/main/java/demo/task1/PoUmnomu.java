package demo.task1;

import demo.task3.Profiling;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PoUmnomu {

    public static void main(String[] args) {
        Stream.iterate(1, i -> ++i)
                .limit(100)
                .peek(i -> System.out.println(i % 15 == 0 ? "CodeInside" : i % 3 == 0 ? "Code" : i % 5 == 0 ? "Inside" : i))
                .collect(Collectors.toList());
    }

    @Profiling
    public static void sleep() throws InterruptedException {
        Thread.sleep(2000);
    }
}
