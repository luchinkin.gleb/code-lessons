package demo.task1;

public class PoTupomu {
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 != 0 && i % 5 != 0) {
                System.out.println(i);
            } else {
                if (i % 3 == 0) {
                    System.out.print("Code");
                }
                if (i % 5 == 0) {
                    System.out.print("Inside");
                }
                System.out.println();
            }
        }
    }
}
