package demo.task1;

public class WithoutIf {
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            System.out.println(i % 15 == 0 ? "CodeInside" : i % 3 == 0 ? "Code" : i % 5 == 0 ? "Inside" : i);
        }
    }
}
