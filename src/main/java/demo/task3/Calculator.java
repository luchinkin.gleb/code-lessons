package demo.task3;

public class Calculator {
    @Profiling
    public static void calculate() {
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.println("Числа фибоначи: ");
        System.out.print(n0 + " " + n1 + " ");
        for (int i = 3; i <= 11; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
        }
        System.out.println();
    }

    @Profiling
    public static void printString() throws InterruptedException {
        System.out.println("дальше спим 4 сек");
        Thread.sleep(4000);
    }
}

