package demo.threads;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;

public class Main {
    public static void main(String[] args) throws IOException {

        File src = new File("src.txt");
        int count = countOfLines(src);
        var executorService = Executors.newFixedThreadPool(2 * count);
        Phaser phaser = new Phaser(1);
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(src))) {
            for (int i = 0; i <= countOfLines(src); i++) {
                var line = bufferedReader.readLine();
                executorService.submit(new MyThread(line, phaser));
                phaser.arriveAndAwaitAdvance();
                phaser.arriveAndDeregister();
            }
        }
        executorService.shutdown();
    }

    private static int countOfLines(File file) {
        int count = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            var line = bufferedReader.readLine();
            while (line != null) {
                count++;
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
