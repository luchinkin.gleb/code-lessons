package demo.threads;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Phaser;

public class MyThread implements Runnable {

    private final String line;
    private final Phaser phaser;

    public MyThread(String line, Phaser phaser) {
        this.line = line;
        this.phaser = phaser;
        phaser.register();
    }

    @Override
    public void run() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("dst.txt", true))) {
            var changedLine = line + " " + line.length();
            System.out.println(changedLine);
            bufferedWriter.write(line + " " + line.length() + "\n");
            phaser.arriveAndDeregister();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
