package demo;

import demo.task3.Profiling;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo {

    private static final String message = "Метод %s класса %s работал %d ms";

    public static void main(String[] args) {
        String packageName = Demo.class.getPackageName();

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Stream<URL> urls = classLoader.resources(packageName);
        List<File> dirs = new ArrayList<>();

        urls.forEach(url -> dirs.add(new File(url.getPath())));

        List<Class> classes = dirs.stream()
                .flatMap(file -> findClasses(file, packageName)
                        .stream()).collect(Collectors.toList());

        classes.forEach(clazz -> Arrays.stream(clazz.getMethods())
                .filter(method -> method.isAnnotationPresent(Profiling.class))
                .forEach(method -> {
                    long time = System.currentTimeMillis();
                    try {
                        method.invoke(null);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    System.out.printf((message) + "%n", method.getName(),
                            clazz.getName(),
                            System.currentTimeMillis() - time);
                }));
    }

    private static List<Class> findClasses(File directory, String packageName) {
        if (!directory.exists()) {
            return Collections.emptyList();
        }

        List<Class> classes = new ArrayList<>();
        File[] files = directory.listFiles();

        if (files == null) {
            return Collections.emptyList();
        }

        for (File file : files) {
            if (file.isDirectory()) {
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                try {
                    classes.add(Class.forName(packageName + '.'
                            + file.getName().substring(0, file.getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
        return classes;
    }
}
