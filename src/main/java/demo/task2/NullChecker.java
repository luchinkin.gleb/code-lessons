package demo.task2;

import java.util.function.Function;

public class NullChecker<T> {

    private final T value;

    private NullChecker(T value) {
        this.value = value;
    }

    public static <T> NullChecker<T> from(T value) {
        return new NullChecker<>(value);
    }

    public T ifNull(T anotherValue) {
        if (this.value == null) {
            return anotherValue;
        }
        return this.value;
    }

    public T ifNullThrow(Exception exception) throws Exception {
        if (this.value == null) {
            throw exception;
        }
        return this.value;
    }

    public <A> NullChecker<A> map(Function<? super T, ? extends A> mapper) {
        if (this.value != null) {
            return new NullChecker<>(mapper.apply(this.value));
        }
        return new NullChecker<>(null);
    }
}

