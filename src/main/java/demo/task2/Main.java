package demo.task2;

public class Main {
    public static void main(String[] args) throws Exception {

        User nullUser = null;
        var user1 = NullChecker.from(nullUser)
                .map(u -> {
                    u.setName("первый");
                    u.setRole("укроп");
                    return u;
                })
                .ifNull(new User("второй", "укроп"));

        System.out.println(user1.getName() + " " + user1.getRole());

        User notNullUser = new User();
        var user2 = NullChecker.from(notNullUser)
                .map(u -> {
                    u.setName("первый");
                    u.setRole("укроп");
                    return u;
                })
                .ifNull(new User("второй", "укроп"));

        System.out.println(user2.getName() + " " + user2.getRole());

        var user3 = NullChecker.from(nullUser)
                .map(u -> {
                    u.setName("первый");
                    u.setRole("укроп");
                    return u;
                })
                .ifNullThrow(new RuntimeException("User is null"));

        System.out.println(user3.getName() + " " + user3.getRole());
    }
}
